## 功能介绍 
随着审美和健康管理水平不断提高，美容美体十分受到年轻女性群体青睐。美容养生馆预约小程序，可以实现美容机构时间有序安排，有利于做好资源配置工作。通过对于美容项目、技师、服务内容、图片等的展示，用户可以在小程序上面更加深入的了解服务描述、服务价格、预期服务时间等内容；用户选择好服务时段，填写个人信息生成预约单，合理安排自己的时间。本项目前后端完整代码包括本店动态，美容常识，项目预约，技师预约，个人中心，收藏和历史记录，后台预约管理，预约时段设置，名单管理，名单导出，预约签到核销，用户管理，内容资讯管理等模块，采用腾讯提供的小程序云开发解决方案，无须服务器和域名。
 
- 美容项目预约管理：开始/截止时间/人数均可灵活设置，可以自定义客户预约填写的数据项
- 美容项目预约凭证：支持线下到场后校验签到/核销/二维码自助签到等多种方式
- 详尽的预约数据：支持预约名单数据导出Excel，打印

![输入图片说明](demo/qr.png)
 

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信: 
 
![输入图片说明](demo/1.png)


## 演示 
 
![输入图片说明](demo/qr.png)

## 安装

- 安装手册见源码包里的word文档




## 截图
![输入图片说明](demo/1%E9%A6%96%E9%A1%B5.png)

![输入图片说明](demo/2%E6%9C%AC%E5%BA%97%E5%8A%A8%E6%80%81.png)
![输入图片说明](demo/3%E4%BF%9D%E5%85%BB%E7%9F%A5%E8%AF%86.png)
 ![输入图片说明](demo/4%E6%97%A5%E5%8E%86.png)
![输入图片说明](demo/5%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/6%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/7%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/8%E9%A2%84%E7%BA%A6.png)

## 后台管理系统截图 

![输入图片说明](demo/9%E5%90%8E%E5%8F%B0%E9%A6%96%E9%A1%B5.png)

![输入图片说明](demo/10%E5%90%8E%E5%8F%B0-%E5%86%85%E5%AE%B9.png)
![输入图片说明](demo/11%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/12%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/13%E5%90%8E%E5%8F%B0-%E6%97%B6%E6%AE%B5.png)
![输入图片说明](demo/14%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6.png)